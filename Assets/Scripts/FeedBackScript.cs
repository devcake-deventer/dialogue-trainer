﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GivenAnswer
{
    private string answer;
    public string Answer
    {
        get { return answer; }
    }
    private string feedback;
    public string Feedback
    {
        get { return feedback; }
    }
    private AnswerScript.AnswerType answerType;
    public AnswerScript.AnswerType AnswerType
    {
        get { return answerType; }
    }

    public GivenAnswer(string _answer, string _feedback, AnswerScript.AnswerType _answerType)
    {
        this.answer = _answer;
        this.feedback = _feedback;
        this.answerType = _answerType;
    }
}

public class FeedBackScript : MonoBehaviour {

    [SerializeField]
    private List<GivenAnswer> answersGiven = new List<GivenAnswer>();

    [SerializeField]
    private List<FeedbackCard> feedbackCards = new List<FeedbackCard>();

    public GameObject feedbackCardPrefab;

    public delegate void OnSwipeCards(int dir);
    public OnSwipeCards onSwipCards;

    public Image fillNext;
    public Image fillPrevious;

    [SerializeField]
    private Image fillBar;

    private bool startFill, answerLocked;
    [SerializeField]
    private int swipeCurrent = 1, swipeMax;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        //SetButtonsVisible(false);
    }

    void Start()
    {
        print("hoi");
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }
    
    void Update()
    {
		#if UNITY_EDITOR        //debugging
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < 10; i++)
            {
                GivenAnswer answer = new GivenAnswer("blablbalbalbla", "fhjwbhjwbdjhbewdw jwendwjendjwek w djkwnednwkjdnwjke djw djkwednbwjndkj jewndwjkndkjwend wedjnwejndwjkdnwejndkjwendwd\n\n\n\n hbdhewbdhewdjhwbjdbwhedwbdhbwd hewbdbwjhd whedbwbed", AnswerScript.AnswerType.PARTIALLY_CORRECT);
                answersGiven.Add(answer);
            }
            SetButtonsVisible(true);
            ShowFeedback();
        }
		#endif
        if (startFill && fillBar != null)
        {
            if (!answerLocked)
            {
                fillBar.fillAmount += Time.deltaTime;
                if (fillBar.fillAmount >= 1 && onSwipCards != null)
                {
                    answerLocked = true;
                    int dir = (fillBar == fillPrevious) ? -1 : 1;
                    if (swipeCurrent + dir >= 1 && swipeCurrent + dir <= swipeMax)
                    {
                        swipeCurrent += dir;
                        onSwipCards(dir);
                    }
                }
            } else
            {
                print(!CheckIftweening());
                answerLocked = CheckIftweening();
            }
        }
    }

    private bool CheckIftweening()
    {
        foreach (FeedbackCard card in feedbackCards)
            if (card.Tweening) return true;

        return false;
    }

    private void SetButtonsVisible(bool visible)
    {
        print(this);
        FeedBackScript fbs = gameObject.GetComponent<FeedBackScript>();
        print(this);
        for (int i = 0; i < fbs.transform.childCount; i++)
        {
            this.transform.GetChild(i).gameObject.SetActive(visible);
        }
    }

    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        if (scene.name == "Feedback")
        {
            SetButtonsVisible(true);
            ShowFeedback();
        }
    }

    /// <summary>
    /// adds the answer to the list of feedback to show at the end.
    /// </summary>
    public void AddAnswerToFeedback(AnswerScript answer)
    {
        GivenAnswer givenAnswer = new GivenAnswer(answer.AnswerText, answer.Feedback, answer.AnswerTypeEnum);
        answersGiven.Add(givenAnswer);
    }

    private void ShowFeedback()
    {
        int i = 0;
        foreach(GivenAnswer answer in answersGiven)
        {
            GameObject feedbackCard = GameObject.Instantiate(feedbackCardPrefab, this.transform);
            feedbackCard.transform.Translate(new Vector3((4.5f * i), 0f, 0f));
            FeedbackCard cardScript = feedbackCard.GetComponent<FeedbackCard>();
            
            cardScript.DisplayFeedback(answer, i, this);
            feedbackCards.Add(cardScript);

            i++;
        }
        swipeMax = answersGiven.Count;
    }

    /// <summary>
    /// Start the filling process of one of the asnwers
    /// </summary>
    public void OnGazeOver(Image fillBox)
    {
        fillBar = fillBox;
        startFill = true;
    }

    /// <summary>
    /// unfills the answer
    /// </summary>
    public void OnGazeOut()
    {
        startFill = false;
        fillBar.fillAmount = 0f;
    }

    public void OnDestroy()
    {
        print("destroy adn remove event handler");
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }
    
}
