﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(AudioSource))]
public class AnswerScript : MonoBehaviour {

    [SerializeField]
    private AudioClip audioClip;
    private AudioSource audioSource;

    //enums
    public enum AnswerType
    {
        CORRECT,
        PARTIALLY_CORRECT,
        INCORRECT
    };

    //fields
    [SerializeField]
    private AnswerType answerType;

    public AnswerType AnswerTypeEnum
    {
        get
        {
            return this.answerType;
        }
        set
        {
            answerType = value;
        }
    }

    [SerializeField]
    private bool startFill;

    [SerializeField]
    private string answerText = "Empty";

    public string AnswerText
    {
        get
        {
            return answerText;
        }
        set
        {
            answerText = value;
        }
    }

    [SerializeField]
    private string sceneToLoad = "";

    public string SceneToLoad
    {
        get
        {
            return sceneToLoad;
        }
    }
    
    [SerializeField]
    private Text questionText;

    [SerializeField]
    private string feedback;

    public string Feedback
    {
        get {
            return feedback;
        }
        set
        {
            feedback = value;
        }
    }

    [SerializeField]
    private Image fillBar;
    
    private bool answerLocked;

    //delegates
    public delegate void AnswerChosen(AnswerScript answer);
    public AnswerChosen onChosenAnswer;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();

        questionText.text = answerText;
        Debug.Assert(fillBar != null);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (startFill && fillBar != null)
        {
            fillBar.fillAmount += Time.deltaTime * 0.7f;
            if (fillBar.fillAmount >= 1 && onChosenAnswer != null && !answerLocked)
            {
                answerLocked = true;
                onChosenAnswer(this);
            }
        }
    }

    /// <summary>
    /// highlight/move forward the complete answer
    /// </summary>
    public void HighlightAnswer()
    {
        this.gameObject.transform.DOLocalMoveZ(2.5f, 0.3f);
		audioSource.PlayOneShot(audioClip);
    }

    /// <summary>
    /// unhighlight/move back the complete answer
    /// </summary>
    /// <param name="GO"></param>
    public void UnHightlightAnswer()
    {
        this.gameObject.transform.DOLocalMoveZ(2.9f, 0.3f);
    }

    /// <summary>
    /// Start the filling process of one of the asnwers
    /// </summary>
    public void OnGazeOver()
    {
        startFill = true;
    }

    /// <summary>
    /// unfills the answer
    /// </summary>
    public void OnGazeOut()
    {
        if (answerLocked) return;
        startFill = false;
        fillBar.fillAmount = 0f;
    }
}
