﻿using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System.Collections;

[RequireComponent(typeof(VideoPlayer))]
[RequireComponent(typeof(AudioSource))]
public class VideoController : MonoBehaviour
{
    [SerializeField]
    private bool startFill;
    [SerializeField]
    private Image fillBar;
    [SerializeField]
    private LoadingOverlay overlay;
    [SerializeField]
    private bool endOfTraining;
    [SerializeField]
    private Canvas playBtn;
    [SerializeField]
    private Canvas pauseBtn;


    VideoPlayer player;
    AudioSource audioSource;
    Text errorMessageText;
    Text frameDropMessageText;

    bool answerLocked;
    int frameDrops = 0;

    // Use this for initialization
    void Start()
    {
        playBtn.gameObject.SetActive(false);

        player = GetComponent<VideoPlayer>();
        audioSource = GetComponent<AudioSource>();
        //errorMessageText = GameObject.Find("ErrorMessageText").GetComponent<Text>();
        //frameDropMessageText = GameObject.Find("FrameDropText").GetComponent<Text>();

        player.SetTargetAudioSource(0, audioSource);

        player.frameDropped += Player_frameDropped;
        player.errorReceived += Player_errorReceived;
        player.loopPointReached += Player_loopPointReached;
        player.prepareCompleted += HandlePrepared;

        player.url = (SceneManager.GetActiveScene().name == "Feedback") ? "" : "https://www.devcake.nl/dialoguetrainer/low/" + SceneManager.GetActiveScene().name + ".mp4";

        player.Prepare();
    }

    void Update()
    {
        if (startFill && fillBar != null)
        {
            fillBar.fillAmount += Time.deltaTime * 0.7f;
            if (fillBar.fillAmount >= 1 && !answerLocked)
            {

                switch (fillBar.name)
                {
                    case "FillbarPlay":
                        //Play button
                        if (!player.isPlaying)
                            player.Play();

                        playBtn.gameObject.SetActive(false);
                        pauseBtn.gameObject.SetActive(true);
                        fillBar.fillAmount = 0;
                        break;
                    case "FillbarPause":
                        //Pause button
                        if (player.isPlaying)
                            player.Pause();

                        playBtn.gameObject.SetActive(true);
                        pauseBtn.gameObject.SetActive(false);
                        fillBar.fillAmount = 0;
                        break;
                    case "FillbarReplay":
                        //Replay button
                        answerLocked = true;
                        StartCoroutine(ReloadScene());
                        break;
                    default:
                        break;
                }
            }
        }
        //frameDropMessageText.text = "frame drops: " + frameDrops.ToString();
    }

    private void Player_loopPointReached(VideoPlayer source)
    {
        if (SceneManager.GetActiveScene().name == "1.1.A")
            //SceneManager.LoadScene("2.1.A");
            SceneManager.LoadSceneAsync("2.1.a");
        else if (endOfTraining)
        {
            SceneManager.LoadScene("Feedback");
        }
    }

    private void Player_errorReceived(VideoPlayer source, string message)
    {
        //if (errorMessageText != null)
        //    errorMessageText.text = message;
    }

    private void Player_frameDropped(VideoPlayer source)
    {
        frameDrops++;
    }

    private void HandlePrepared(VideoPlayer source)
    {
        if (GameObject.FindGameObjectWithTag("Overlay") != null)
        {
            overlay = GameObject.FindGameObjectWithTag("Overlay").GetComponent<LoadingOverlay>();
            overlay.SetVariables();
            overlay.FadeOut();
        }
        
        source.Play();
        if (audioSource != null)
        {
            //audioSource.Play();
        }
    }

    public void OnGazeOver(Image fillImage)
    {
        fillBar = fillImage;
        startFill = true;
    }

    public void OnGazeOut()
    {
        startFill = false;
        fillBar.fillAmount = 0;
    }

    private IEnumerator ReloadScene()
    {
        if (overlay != null)
        {
            overlay.FadeIn();
            string sceneToLoad = SceneManager.GetActiveScene().name;
            yield return new WaitForSeconds(2);
            //SceneManager.LoadScene(sceneToLoad);
            AsyncOperation async = SceneManager.LoadSceneAsync(sceneToLoad);
            print(async.progress);
            yield return async;
        }
        else
        {
            string sceneToLoad = SceneManager.GetActiveScene().name;
            yield return new WaitForSeconds(2);
            //SceneManager.LoadScene(sceneToLoad);
            AsyncOperation async = SceneManager.LoadSceneAsync(sceneToLoad);
            yield return async;
        }
    }
}
