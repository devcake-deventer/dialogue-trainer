﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FeedbackCard : MonoBehaviour {

    public Text feedbacktext;
    public Image backGround;

    private FeedBackScript cardHolder;
    private Color bgColor;
    private bool tweening;

    public bool Tweening
    {
        get { return tweening; }
    }

    public void DisplayFeedback(GivenAnswer answer, int i, FeedBackScript _cardHolder)
    {
        feedbacktext.text = i.ToString() + ") Je koos voor: " + answer.Answer + "\n\n" + answer.Feedback;
        cardHolder = _cardHolder;
        
        switch (answer.AnswerType)
        {
            case AnswerScript.AnswerType.CORRECT:
                print("correct");
                bgColor = new Color(0.6156862745098039f, 0.7647058823529412f, 0.3764705882352941f);
                break;
            case AnswerScript.AnswerType.INCORRECT:
                print("incorrect");
                bgColor = new Color(0.7647058823529412f, 0.3764705882352941f, 0.3764705882352941f);
                break;
            case AnswerScript.AnswerType.PARTIALLY_CORRECT:
                print("part correct");
                bgColor = new Color(0.7647058823529412f, 0.6745098039215686f, 0.3764705882352941f);
                break;
            default:
                print("default");
                bgColor = new Color(0.7647058823529412f, 0.6745098039215686f, 0.3764705882352941f);
                break;
        }

        backGround.color = bgColor;
        print(backGround.color);
        cardHolder.onSwipCards += SwipeCard;
    }

    public void SwipeCard(int dir = 1)
    {
        tweening = true;
        this.gameObject.transform.DOMoveZ((4.5f * dir), 0.7f).SetRelative(true).OnComplete(tweenCompleted);
    }

    private void tweenCompleted()
    {
        tweening = false;
    }
}
