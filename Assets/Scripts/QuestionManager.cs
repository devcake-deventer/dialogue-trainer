﻿using System.Linq;
using System.Collections;

using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

using DG.Tweening;

public class QuestionManager : MonoBehaviour
{
    VideoPlayer videoPlayer;
    LoadingOverlay overlay;

    //fields
    [SerializeField]
    private AnswerScript[] answers;

    [SerializeField]
    private GameObject overlayObject;

    //public variables
    public GameObject overlayPrefab;
    public Transform cameraObject;

    void Start()
    {
        if (cameraObject == null)
            cameraObject = GameObject.Find("Main Camera").transform;

        Debug.Assert(cameraObject != null);

        overlayObject = GameObject.Instantiate(overlayPrefab, cameraObject);
        overlay = overlayObject.GetComponent<LoadingOverlay>(); //GameObject.Find("LoadingOverlay").gameObject.GetComponent<LoadingOverlay>();
        
        videoPlayer = GameObject.Find("Video Manager").GetComponent<VideoPlayer>();
        videoPlayer.loopPointReached += EndReached;

        answers = GetComponentsInChildren<AnswerScript>().ToArray();
        foreach (AnswerScript answerOption in answers)
        {
            answerOption.onChosenAnswer += AnswerWasChosen;
            answerOption.gameObject.SetActive(false);
        }
        
    }

    private void Update()
    {
#if UNITY_EDITOR
        if(Input.GetKeyDown("space") )
        {
            SpawnQuestions();
        }
#endif
    }

    private void EndReached(VideoPlayer v)
    {
        SpawnQuestions();
    }

    public void SpawnQuestions()
    {
        foreach(AnswerScript answer in answers)
        {
            answer.gameObject.SetActive(true);
            answer.gameObject.transform.DOMoveZ(10, 0.7f).From(true);
        }
    }

    void AnswerWasChosen(AnswerScript answer)
    {
        FindObjectOfType<FeedBackScript>().AddAnswerToFeedback(answer);

        if (answer.SceneToLoad != "")
            StartCoroutine(SelectAndContinue(answer));
    }

    IEnumerator SelectAndContinue(AnswerScript answer)
    {
        foreach(AnswerScript answerOption in answers.Where(a => a != answer))
        {
            answerOption.gameObject.transform.DOMoveZ(10, 0.7f).SetRelative(true);
            overlay.FadeIn();
        }
        yield return new WaitForSeconds(2);
        
        SceneManager.LoadScene(answer.SceneToLoad);
    }
}
