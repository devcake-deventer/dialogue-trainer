﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartController : MonoBehaviour {

	[SerializeField]
	private bool startFill;
	[SerializeField]
	private Image fillBar;
	[SerializeField]
	private LoadingOverlay overlay;

    private bool answerLocked;

    // Use this for initialization
    void Start ()
    {
        Debug.Assert(overlay != null);
        overlay.SetVariables ();
		overlay.FadeOut ();
	}
	
	// Update is called once per frame
	void Update () {
		if(startFill && fillBar != null)
		{
			fillBar.fillAmount += Time.deltaTime * 0.7f;
			if (fillBar.fillAmount >= 1 && !answerLocked)
			{
                answerLocked = true;
				StartCoroutine(LoadScene());
			}
		}
	}

	private IEnumerator LoadScene() {
        Debug.Assert(overlay != null);

        Destroy(GameObject.Find("FeedbackObjectDDOL"));

        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(false);


        overlay.FadeIn();
        yield return null;

        AsyncOperation async = SceneManager.LoadSceneAsync("1.1.A");
        async.allowSceneActivation = false;

        while (!async.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(async.progress / 0.9f);

            // Loading completed
            if (Mathf.Approximately(async.progress, 0.9f))
            {
                async.allowSceneActivation = true;
            }

            yield return null;
        }
        
	}

	public void OnGazeOver() {
		startFill = true;
	}

	public void OnGazeOut() {
		startFill = false;
		fillBar.fillAmount = 0;
	}
}
