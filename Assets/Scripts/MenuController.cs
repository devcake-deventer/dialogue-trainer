﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using DG.Tweening;

public class MenuController : MonoBehaviour {
    [SerializeField]
    private string sceneToLoad;
    [SerializeField]
    private bool startFill;
    [SerializeField]
    private Image fillBar;
    [SerializeField]
    private LoadingOverlay overlay;

    private bool answerLocked;

    private void Start()
    {
        overlay.SetVariables();
        overlay.FadeOut();
    }

    private void Update()
    {
        if(startFill && fillBar != null)
        {
            fillBar.fillAmount += Time.deltaTime * 0.7f;
            if (fillBar.fillAmount >= 1 && sceneToLoad != null && !answerLocked)
            {
                answerLocked = true;
                StartCoroutine(LoadNewScene());
            }else if (sceneToLoad == null)
            {
                answerLocked = true;
                StartCoroutine(QuitApplication());
            }
        }
    }

     
    private IEnumerator LoadNewScene()
    {
        print("starting asynch load");
        Debug.Assert(overlay != null);

        overlay.FadeIn();
        yield return null;

		AsyncOperation async = SceneManager.LoadSceneAsync(sceneToLoad);
        async.allowSceneActivation = false;

        while (!async.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(async.progress / 0.9f);
            Debug.Log("Loading progress: " + (progress * 100) + "%");

            // Loading completed
            if (Mathf.Approximately(async.progress, 0.9f))
            {
                Debug.Log("Starting");
                async.allowSceneActivation = true;
            }

            yield return null;
        }

    }

    private IEnumerator QuitApplication()
    {
        overlay.FadeIn();
        yield return new WaitForSeconds(2);
        Application.Quit();
    }

    public void OnGazeOver()
    {
        this.gameObject.transform.DOLocalMoveZ(-2.1f, 0.3f);
        startFill = true;
    }

    public void OnGazeOut()
    {
        this.gameObject.transform.DOLocalMoveZ(-2.3f, 0.3f);
        startFill = false;
        fillBar.fillAmount = 0;
    }
}
